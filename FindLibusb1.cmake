# FindLibusb1.cmake
#
# CMake module to find libusb 1.0 package
# (http://libusb.info)
#

if (MSVC)
  set (Libusb1_LIBRARY_NAME libusb-1.0)
  if (CMAKE_CL_64)
    set (Libusb1_RELEASE_LIBRARY_PATH_SUFFIXES
      MS64/dll          # official Windows binaries (release only)
      x64/Release/dll)  # source distribution
    set (Libusb1_DEBUG_LIBRARY_PATH_SUFFIXES x64/Debug/dll)
  else ()
    set (Libusb1_RELEASE_LIBRARY_PATH_SUFFIXES MS32/dll Win32/Release/dll)
    set (Libusb1_DEBUG_LIBRARY_PATH_SUFFIXES Win32/Debug/dll)
  endif ()
else ()
  set (Libusb1_LIBRARY_NAME usb-1.0)
endif ()

find_package (PkgConfig QUIET)
if (PKG_CONFIG_FOUND)
  pkg_check_modules (Libusb1_PC libusb-1.0)
endif ()

find_path (Libusb1_INCLUDE_DIR libusb.h
  HINTS ${Libusb1_DIR} $ENV{Libusb1_DIR} ${Libusb1_PC_INCLUDE_DIRS}
  PATH_SUFFIXES include/libusb-1.0  # official Windows binaries 
                libusb)             # source distribution
mark_as_advanced (Libusb1_INCLUDE_DIR)

find_library (Libusb1_LIBRARY_RELEASE ${Libusb1_LIBRARY_NAME}
  HINTS ${Libusb1_DIR} $ENV{Libusb1_DIR} ${Libusb1_PC_LIBRARY_DIRS}
  PATH_SUFFIXES ${Libusb1_RELEASE_LIBRARY_PATH_SUFFIXES})
mark_as_advanced (Libusb1_LIBRARY_RELEASE)

find_library (Libusb1_LIBRARY_DEBUG ${Libusb1_LIBRARY_NAME}
  HINTS ${Libusb1_DIR} $ENV{Libusb1_DIR} ${Libusb1_PC_LIBRARY_DIRS}
  PATH_SUFFIXES ${Libusb1_DEBUG_LIBRARY_PATH_SUFFIXES})
if (NOT Libusb1_LIBRARY_DEBUG)
  set (Libusb1_LIBRARY_DEBUG ${Libusb1_LIBRARY_RELEASE})
endif ()
mark_as_advanced (Libusb1_LIBRARY_DEBUG)

set (Libusb1_INCLUDE_DIRS ${Libusb1_INCLUDE_DIR})
set (Libusb1_LIBRARIES
  optimized ${Libusb1_LIBRARY_RELEASE}
  debug ${Libusb1_LIBRARY_DEBUG})

include (FindPackageHandleStandardArgs)
find_package_handle_standard_args (Libusb1
  FOUND_VAR Libusb1_FOUND
  REQUIRED_VARS Libusb1_LIBRARY_RELEASE Libusb1_INCLUDE_DIR)
