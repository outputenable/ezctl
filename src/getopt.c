/*
   Copyright (C) 2012, 2014, 2015, 2016 Oliver Ebert

   Permission is hereby granted, free of charge, to any person obtaining a
   copy of this software and associated documentation files (the "Software"),
   to deal in the Software without restriction, including without limitation
   the rights to use, copy, modify, merge, publish, distribute, sublicense,
   and/or sell copies of the Software, and to permit persons to whom the
   Software is furnished to do so, subject to the following conditions:

   The above copyright notice and this permission notice shall be included in
   all copies or substantial portions of the Software.

   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
   IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
   FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL
   THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
   LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
   FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
   DEALINGS IN THE SOFTWARE.

   Except as contained in this notice, the name of an author or copyright
   holder shall not be used in advertising or otherwise to promote the sale,
   use or other dealings in this Software without prior written authorization
   from the author or copyright holder.
*/

#include "getopt.h"

#include <assert.h>
#include <stdio.h>
#include <string.h>

char *optarg;
int optind = 1;
int opterr = 1;
int optopt;

int
getopt(int argc, char * const argv[], const char *optstring)
	{
	static size_t pos;
	const char *cp;
	int c;
	const char *opt;

	assert(argv != NULL);
	assert(optstring != NULL);

	if (optind < 0 || optind >= argc)
		goto done;
	cp = argv[optind];
	if (cp == NULL)
		/* argv[optind] is a NULL pointer */
		goto done;
	if (pos == 0)
		{
		if (cp[0] != '-' || cp[1] == '\0')
			/* *argv[optind] is not the character -
			 * argv[optind] points to the string "-" */
			goto done;
		if (cp[1] == '-' && cp[2] == '\0')
			{
			/* argv[optind] points to the string "--" */
			++optind;
 done:
			c = -1;
			goto finally;
			}
		pos = 1;
		}
	else
		assert(pos < strlen(cp));
	cp += pos;

	optopt = c = *cp++;
	if (c == ':' || (opt = strchr(optstring, c)) == NULL)
		{
		if (*optstring != ':' && opterr)
			fprintf(stderr, "%s: illegal option -- %c\n",
			    argv[0] != NULL ? argv[0] : "getopt()", c);
		c = '?';
		goto nextopt;
		}

	if (opt[1] != ':')
 nextopt:
		if (*cp == '\0')
			{
			++optind;
			pos = 0;
			}
		else
			++pos;
	else  /* option takes an argument */
		{
		++optind;
		pos = 0;
		if (*cp != '\0')
			optarg = (char *)cp;
		else
			{
			optarg = argv[optind];
			if (++optind > argc)
				if (*optstring != ':')
					{
					if (opterr)
						fprintf(stderr,
						    "%s: option requires an "
						    " argument -- %c\n",
						    argv[0] != NULL ? argv[0]
						    : "getopt()",
						    c);
					c = '?';
					}
				else
					c = ':';
			}
		}
 finally:
	return c;
	}
