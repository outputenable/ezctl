/*
   Copyright (C) 2012, 2013, 2014, 2015, 2016, 2017 Oliver Ebert

   Permission is hereby granted, free of charge, to any person obtaining a
   copy of this software and associated documentation files (the "Software"),
   to deal in the Software without restriction, including without limitation
   the rights to use, copy, modify, merge, publish, distribute, sublicense,
   and/or sell copies of the Software, and to permit persons to whom the
   Software is furnished to do so, subject to the following conditions:

   The above copyright notice and this permission notice shall be included in
   all copies or substantial portions of the Software.

   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
   IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
   FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL
   THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
   LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
   FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
   DEALINGS IN THE SOFTWARE.

   Except as contained in this notice, the name of an author or copyright
   holder shall not be used in advertising or otherwise to promote the sale,
   use or other dealings in this Software without prior written authorization
   from the author or copyright holder.
*/

#if defined _MSC_VER
# define _CRT_SECURE_NO_WARNINGS  /* suppress C4996 */
#endif

#include "config.h"

#include <assert.h>
#include <errno.h>
#include <limits.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#ifdef HAVE_GETOPT
# include <unistd.h>
#else
# include "getopt.h"
#endif

#include <hoff/ihex.h>
#include <libusb.h>

#if !defined UINT16_MAX
# define UINT16_MAX  65535
#endif

#if defined _WIN32
# define DIR_SEP  '\\'
#else
# define DIR_SEP  '/'
#endif

#define USB_DEVICE_KEY_STRSIZ  33  /* including the terminating null byte */

#define VENDOR_ID_CYPRESS  0x04b4
#define PRODUCT_ID_FX2LP   0x8613

#define ACC_WR  (1 << 0)  /* write access */
#define ACC_RD  (1 << 1)  /* read access */

struct usb_device_key
	{
	int bus_num;
	int dev_num;
	long int vendor_id;
	long int product_id;
	};

struct memory_region
	{
	unsigned short start;
	unsigned short end;  /* inclusive */
	unsigned short flags;
	};


static char const *progname;

static int verbosity;

static struct usb_device_key target_key =
	{
	-1,  /* don't care */
	-1,
	VENDOR_ID_CYPRESS,
	PRODUCT_ID_FX2LP
	};

static char buffer[(((8 * 1024 - 1) / BUFSIZ + 1) * BUFSIZ) + 1];
    /* one extra for terminating null */

static libusb_device_handle *target;
static const struct memory_region *memory_map;

static const struct memory_region fx2lp_default_memory_map[] =
	{
	{ 0x0000, 0x3fff, ACC_RD | ACC_WR },
	{ 0xe000, 0xe1ff, ACC_RD | ACC_WR },
	{ 0xe600, 0xe600,          ACC_WR },
	{ 0 }
	};

static int parse_args(int argc, char *argv[]);
static void print_usage(FILE *stream);

static libusb_device_handle *open_target_usb_device
    (struct usb_device_key *key);

static void print_usb_device_key(char *s, const struct usb_device_key *key);
static int usb_device_key_matchp(struct libusb_device *dev, void *ctx);

static int usb_find_device(libusb_device **devptr,
    int (*matchp)(libusb_device *dev, void *ctx), void *ctx,
    libusb_context *session);

static int process_input_file(const char *filename);
static int firmware_download(unsigned long int org, const void *data,
    size_t size, unsigned int flags, void *ctx);

static int ezusb_fx_vendor_request(libusb_device_handle *ezhandle,
    enum libusb_endpoint_direction dir, unsigned char bRequest,
    unsigned short int start_addr, const void *data,
    unsigned short int num_bytes, unsigned int timeout_millis);

static int mdispatch
    (int (*func)(unsigned long int org, const void *data, size_t size,
         unsigned int flags, void *ctx),
     void *ctx, const struct memory_region *mmap, unsigned long int org,
     const void *data, size_t size);

int
main(int argc, char *argv[])
	{
	int status;
	int rc;

	if (argv[0] != NULL)
		{
		progname = strrchr(argv[0], DIR_SEP);
		if (progname == NULL || *++progname == '\0')
			progname = argv[0];
		}
	else
		progname = "ezctl";

	status = EXIT_FAILURE;

	rc = parse_args(argc, argv);
	if (rc != 0)
		goto finally;

	rc = libusb_init(NULL);
	if (rc != 0)
		{
		fprintf(stderr, "%s: error initializing libusb: %s [%d]\n",
		    progname, libusb_error_name(rc), rc);
		goto finally;
		}

	target = open_target_usb_device(&target_key);
	if (target == NULL)
		goto libusb_exit;

	memory_map = fx2lp_default_memory_map;

	assert(optind > 0 && optind < argc);
	do
		if (process_input_file(argv[optind]) != 0)
			goto close_target;
		while (++optind < argc);

	status = EXIT_SUCCESS;

 close_target:
	/* do not bother with re-attaching a kernel driver here */
	(void)libusb_release_interface(target, 0);
	libusb_close(target);
 libusb_exit:
	libusb_exit(NULL);
 finally:
	exit(status);
	}

static int
parse_args(int argc, char *argv[])
	{
	int c;
	int rc;

	assert(argv != NULL);

	while ((c = getopt(argc, argv, "v")) != -1)
		switch (c)
			{
		case 'v':
			++verbosity;
			break;
		default:
			goto print_usage;
			}
	if (optind < argc)
		rc = 0;
	else
 print_usage:
		{
		print_usage(stderr);
		rc = -1;
		}
	return rc;
	}

static void
print_usage(FILE *stream)
	{
	assert(stream != NULL);

	fprintf(stream,
	    "usage: %s [OPTIONS] FILE...\n"
	    "\n"
	    "where OPTIONS are:\n"
	    "  -v    increase verbosity\n",
	    progname);
	}

static libusb_device_handle *
open_target_usb_device(struct usb_device_key *key)
	{
	int rc;
	libusb_device *dev;
	libusb_device_handle *handle;
	unsigned int driver_was_attached;

	assert(key != NULL);

	if (verbosity >= 1)
		{
		char key_str[USB_DEVICE_KEY_STRSIZ];

		print_usb_device_key(key_str, key);
		printf("%s: USB device discovery: %s\n", progname, key_str);
		}
	dev = NULL;
	rc = usb_find_device(&dev, usb_device_key_matchp, key, NULL);
	handle = NULL;
	if (rc != 0)
		{
		fprintf(stderr,
		    "%s: error discovering USB devices: %s [%d]\n", progname,
		    libusb_error_name(rc), rc);
		goto finally;
		}
	if (dev == NULL)
		{
		fprintf(stderr, "%s: no matching target USB device found\n",
		    progname);
		goto finally;
		}
	if (verbosity >= 0)
		{
		char key_str[USB_DEVICE_KEY_STRSIZ];

		print_usb_device_key(key_str, key);
		printf("%s: found target USB device: %s\n", progname,
		    key_str);
		}
	rc = libusb_open(dev, &handle);
	libusb_unref_device(dev);
	if (rc != 0)
		{
		fprintf(stderr,
		    "%s: error opening target USB device:"
		    " Bus %.3d Device %.3d: %s [%d]\n",
		    progname, key->bus_num, key->dev_num,
		    libusb_error_name(rc), rc);
		goto finally;
		}
	rc = libusb_detach_kernel_driver(handle, 0);
	if (rc != 0
	    && rc != LIBUSB_ERROR_NOT_FOUND  /* no kernel driver was active */
	    && rc != LIBUSB_ERROR_NOT_SUPPORTED)
		{
		fprintf(stderr,
		    "%s: Bus %.3d Device %.3d:"
		    " Error detaching kernel driver from interface 0:"
		    " %s [%d]\n",
		    progname, key->bus_num, key->dev_num,
		    libusb_error_name(rc), rc);
		goto close;
		}
	driver_was_attached = rc == 0;
	rc = libusb_claim_interface(handle, 0);
	if (rc != 0)
		{
		fprintf(stderr,
		    "%s: Bus %.3d Device %.3d:"
		    " error claiming interface 0: %s [%d]\n",
		    progname, key->bus_num, key->dev_num,
		    libusb_error_name(rc), rc);
		if (driver_was_attached)
			(void)libusb_attach_kernel_driver(handle, 0);
 close:
		libusb_close(handle);
		handle = NULL;
		}
 finally:
	return handle;
	}

static void
print_usb_device_key(char *s, const struct usb_device_key *key)
	{
	char *sp;

	assert(s != NULL);
	assert(key != NULL);
	assert(key->bus_num <= 999);
	assert(key->dev_num <= 999);
	assert(key->vendor_id <= 0xffff);
	assert(key->product_id <= 0xffff);

	sp = s;
	sp += sprintf(sp, key->bus_num >= 0 ? "Bus %.3d" : "Bus *",
	    key->bus_num);
	sp += sprintf(sp, key->dev_num >= 0 ? " Device %.3d" : " Device *",
	    key->dev_num);
	sp += sprintf(sp, key->vendor_id >= 0 ? ": ID %.4x" : ": ID *",
	    (unsigned int)key->vendor_id);
	sp += sprintf(sp, key->product_id >= 0 ? ":%.4x" : ":*",
	    (unsigned int)key->product_id);
	assert(sp - s < USB_DEVICE_KEY_STRSIZ);
	}

static int
usb_device_key_matchp(struct libusb_device *dev, void *ctx)
	{
	struct usb_device_key *key;
	int busnum;
	int devaddr;
	int match;
	int rc;
	struct libusb_device_descriptor desc;

	assert(dev != NULL);
	assert(ctx != NULL);

	key = ctx;
	busnum = libusb_get_bus_number(dev);
	devaddr = libusb_get_device_address(dev);
	match = (key->bus_num < 0 || busnum == key->bus_num)
	    && (key->dev_num < 0 || devaddr == key->dev_num);
	if ( !match)
		goto finally;
	rc = libusb_get_device_descriptor(dev, &desc);
	if (rc == 0)
		{
		match =
		    (key->vendor_id < 0
		     || desc.idVendor == (unsigned int)key->vendor_id)
		    && (key->product_id < 0
		        || desc.idProduct == (unsigned int)key->product_id);
		if (match)
			{
			key->bus_num = busnum;
			key->dev_num = devaddr;
			key->vendor_id = desc.idVendor;
			key->product_id = desc.idProduct;
			}
		}
	else
		{
		fprintf(stderr,
		    "%s: error getting USB device descriptor:"
		    " Bus %.3d Device %.3d: %s [%d]\n",
		    progname, busnum, devaddr, libusb_error_name(rc), rc);
		match = 0;
		}
 finally:
	return match;
	}

static int
usb_find_device(libusb_device **devptr,
  int (*matchp)(libusb_device *dev, void *ctx), void *ctx,
  libusb_context *session)
	{
	libusb_device **list;
	ssize_t cnt;
	int rc;

	assert(devptr != NULL);
	assert(matchp != NULL);

	cnt = libusb_get_device_list(session, &list);
	if (cnt >= 0)
		{
		libusb_device **i;
		libusb_device *dev;

		/* list is terminated with a NULL element */
		for (i = list; (dev = *i) != NULL; ++i)
			if ((*matchp)(dev, ctx))
				{
				libusb_ref_device(dev);
				break;
				}
		*devptr = dev;
		libusb_free_device_list(list, 1);
		rc = 0;
		}
	else
		rc = (int)cnt;
	return rc;
	}

static int
process_input_file(const char *filename)
	{
	FILE *obj;
	int errnum;
	int rc;
	ihex_parser ps;

	if (filename != NULL && strcmp(filename, "-") != 0)
		{
		obj = fopen(filename, "r");
		if (obj != NULL)
			setbuf(obj, NULL);
		else
			{
			errnum = errno;
			fprintf(stderr,
			    "%s: error opening object file: %s: %s [%d]\n",
			    progname, filename, strerror(errnum), errnum);
			rc = -1;
			goto finally;
			}
		}
	else
		{
		filename = "STDIN";
		obj = stdin;
		}

	ps = ihex_ps_new();
	if (ps == NULL)
		{
		errnum = errno;
		fprintf(stderr, "%s: %s [%d]\n", progname, strerror(errnum),
		    errnum);
		rc = -1;
		goto finally;
		}

	/* The same space (well, almost) will be used for input buffering and
	 * binary firmware data. */
	for (;;)
		{
		size_t nread;
		const char *s;
		const char *eos;
		size_t size;
		unsigned long int org;

		nread = fread(buffer, 1, sizeof buffer - 1, obj);
		    /* save one for the null terminator */
		if (nread != 0)
			{
			buffer[nread] = '\0';
			s = buffer;
			eos = s + nread;
			size = 0;
			}
		else
			{
			if (ferror(obj))
				{
				errnum = errno;
				fprintf(stderr,
				    "%s: error reading object file: %s:"
				    " %s [%d]\n",
				    progname, filename, strerror(errnum),
				    errnum);
				}
			else
				{
				assert(feof(obj));
				fprintf(stderr,
				    "%s: illegal end of file: %s:"
				    " object truncated or incomplete\n",
				    progname, filename);
				}
			rc = -1;
			break;
			}
 parse:
		rc = ihex_ps_readcont(&org, buffer, &size, sizeof buffer, &s,
		    ps);
		if (size != 0)
			{
			if (mdispatch(firmware_download, (void *)filename,
			        memory_map, org, buffer, size)
			    == 0)
				{
				size = 0;
				if (rc == IHEX_TYP_DAT)
					goto parse;
				}
			else
				{
				rc = -1;
				break;
				}
			}
		assert(rc != IHEX_TYP_DAT);
		switch (rc)
			{
		case -IHEX_UNDERFLOW:
			assert(*s == '\0');
			while (s != eos)
				/* skip embedded NULs */
				if (*++s != '\0')
					goto parse;
			continue;

		case IHEX_TYP_EOF:
			rc = 0;
			goto teardown;

		case IHEX_TYP_ESA:
		case IHEX_TYP_ELA:
			break;

		case IHEX_TYP_SSA:
		case IHEX_TYP_SLA:
			if (verbosity >= 1)
				printf
				    ("%s: %s:"
				     " skipping Start %s Address record\n",
				     progname, filename,
				     rc == IHEX_TYP_SSA ? "Segment"
				     : "Linear");
			break;

		case -IHEX_BAD_CHECKSUM:
		case -IHEX_FORMAT_ERROR:
		case -IHEX_SYNTAX_ERROR:
		default:
			fprintf(stderr,
			    "%s: %s: error parsing Intel HEX object:"
			    " %s [%d]\n",
			    progname, filename, ihex_strerror(rc), rc);
			rc = -1;
			goto teardown;
			}
		ihex_ps_terminate(ps);
		}
 teardown:
	ihex_ps_free(ps);
	/* didn't open stdin, so won't close it either */
	if (obj != stdin && fclose(obj) != 0 && verbosity >= 1)
		{
		errnum = errno;
		fprintf(stderr,
		    "%s: error closing object file: %s: %s [%d]\n", progname,
		    filename, strerror(errnum), errnum);
		}
 finally:
	return rc;
	}

static int
firmware_download(unsigned long int org, const void *data, size_t size,
    unsigned int flags, void *ctx)
	{
	unsigned int start_addr;
	unsigned int num_bytes;
	int rc;

	assert(org <= UINT16_MAX);
	assert(data != NULL);
	assert(size <= UINT16_MAX);
	assert(ctx != NULL);

	start_addr = (unsigned int)org;
	num_bytes = (unsigned int)size;
	if ( !(flags & ACC_WR))
		{
		fprintf(stderr,
		    "%s: %s[0x%.4X-0x%.4X]: memory area not writable\n",
		    progname, (const char *)ctx, start_addr,
		    start_addr + num_bytes - 1);
		rc = -1;
		goto finally;
		}
	if (verbosity >= 1)
		printf("%s: %s[0x%.4X-0x%.4X]: downloading %u byte(s)\n",
		    progname, (const char *)ctx, start_addr,
		    start_addr + num_bytes - 1, num_bytes);
	rc = ezusb_fx_vendor_request(target, LIBUSB_ENDPOINT_OUT, 0xa0,
	    (unsigned short int)start_addr, data,
	    (unsigned short int)num_bytes, 1000);
	if (rc == (int)num_bytes)
		rc = 0;
	else
		{
		unsigned int end;

		end = start_addr + num_bytes - 1;
		if (rc < 0)
			fprintf(stderr,
			    "%s: %s[0x%.4X-0x%.4X]:"
			    " USB error downloading firmware: %s [%u]\n",
			    progname, (const char *)ctx, start_addr, end,
			    libusb_error_name(rc), rc);
		else
			/* can this even happen? */
			fprintf(stderr,
			    "%s: %s[0x%.4X-0x%.4X]:"
			    " unknown error downloading firmware:"
			    " incomplete USB control OUT transfer:"
			    " %u of %u byte(s)\n",
			    progname, (const char *)ctx, start_addr, end,
			    (unsigned int)rc, num_bytes);
		rc = -1;
		}
 finally:
	return rc;
	}

static int
ezusb_fx_vendor_request(libusb_device_handle *ezhandle,
    enum libusb_endpoint_direction dir, unsigned char bRequest,
    unsigned short int start_addr, const void *data,
    unsigned short int num_bytes, unsigned int timeout_millis)
	{
#if INT_MAX < UINT16_MAX
# error Uh-oh!  INT_MAX < UINT16_MAX (libusb should disapprove, too).
#endif
	int rc;

#if USHORT_MAX > UINT16_MAX
	if (start_addr > UINT16_MAX || num_bytes > UINT16_MAX)
		rc = LIBUSB_ERROR_INVALID_PARAM;
	else
#endif
		rc = libusb_control_transfer
		    (ezhandle,
		     (uint8_t)(dir | LIBUSB_REQUEST_TYPE_VENDOR
		         | LIBUSB_RECIPIENT_DEVICE),
		     (uint8_t)bRequest,
		     (uint16_t)start_addr,
		     0,
		     (unsigned char *)data,
		     (uint16_t)num_bytes,
		     timeout_millis);
	return rc;
	}

static int
mdispatch
    (int (*func)(unsigned long int org, const void *data, size_t size,
         unsigned int flags, void *ctx),
     void *ctx, const struct memory_region *mmap, unsigned long int org,
     const void *data, size_t size)
	{
	const unsigned char *ptr;
	unsigned long int rend;
	int rc;

	assert(func != NULL);
	assert(mmap != NULL);
	assert(0 < size && size <= ULONG_MAX);
	assert(org <= ULONG_MAX - size + 1);
	assert(data != NULL);

	ptr = (const unsigned char *)data;
	rend = ULONG_MAX;  /* inclusive! */
	do
		{
		unsigned int rflags;
		unsigned long int nb;

		assert(mmap->start <= mmap->end);
		assert((unsigned long int)(mmap->end - mmap->start)
		    < ULONG_MAX);
		if (rend + 1 == mmap->start)
			{
			rend = mmap->end;
			rflags = mmap->flags;
			++mmap;
			}
		else  /* make up a region filling the gap */
			{
			/* mmap must be terminated by a dedicated(!) entry
			 * with start == 0, which yields rend == ULONG_MAX.
			 * The resulting region is supposed to cover all
			 * remaining data so that size becomes zero, ending
			 * the loop.  */
			rend = (unsigned long int)mmap->start - 1;
			rflags = 0;
			}

		if (rend < org)
			continue;
		nb = rend - org + 1;
		if (nb > size)
			nb = (unsigned long int)size;
		rc = (*func)(org, ptr, (size_t)nb, rflags, ctx);
		if (rc != 0)
			goto finally;
		org += nb;
		ptr += nb;
		size -= (size_t)nb;
		}
		while (size != 0);
	rc = 0;
 finally:
	return rc;
	}
